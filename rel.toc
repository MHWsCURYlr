\select@language {portuguese}
\contentsline {chapter}{\numberline {1}Introdu\IeC {\c c}\IeC {\~a}o}{3}
\contentsline {chapter}{\numberline {2}Metodologia Empregada}{4}
\contentsline {chapter}{\numberline {3}Descri\IeC {\c c}\IeC {\~a}o da Plataforma Experimental}{5}
\contentsline {section}{\numberline {3.1}Cliente}{5}
\contentsline {section}{\numberline {3.2}Servidor}{5}
\contentsline {chapter}{\numberline {4}An\IeC {\'a}lise}{7}
\contentsline {section}{\numberline {4.1}Facilidade de Implementa\IeC {\c c}\IeC {\~a}o}{7}
\contentsline {section}{\numberline {4.2}Facilidade de Uso}{7}
\contentsline {section}{\numberline {4.3}Desempenho}{7}
\contentsline {section}{\numberline {4.4}Compara\IeC {\c c}\IeC {\~a}o Qualitativa}{8}
\contentsline {section}{\numberline {4.5}Discuss\IeC {\~a}o}{8}
\contentsline {chapter}{\numberline {5}Principais Problemas Encontrados}{9}
\contentsline {chapter}{\numberline {6}Conclus\IeC {\~a}o}{10}
