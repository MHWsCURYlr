#include <time.h>
#include <assert.h>
#include "sort.h"

int main(int argc, char **argv)
{
	int i;
	CLIENT *client;
	sort_in vetor;
	sort_out *vetorResposta;
	double beginTime, endTime;

	if(argc < 3) {
		printf("Usage: %s <adress> <port>\n", argv[0]);
		exit(1);
	}

	for(i = 0; i < VECTOR_SIZE; i++)
		vetor.vetor[i] = i;

	beginTime = clock();
	for(i = 0; i < TESTS_TOTAL; i++) {
#ifdef DEBUG
		printf("client: %i\n", i);
		int j;
		for(j = 0; j < VECTOR_SIZE; j++)
			printf("%i ", vetor.vetor[j]);
		printf("\n");
#endif

		client = clnt_create(argv[1], SORT_PROG, SORT_VERS, SOCKET_TYPE);
		assert(client);
		vetorResposta = sort_proc_1(&vetor, client);
		assert(vetorResposta);
		clnt_destroy(client);
#ifdef DEBUG
		for(j = 0; j < VECTOR_SIZE; j++)
			printf("%i ", vetorResposta->vetor[j]);
		printf("\n");
#endif
	}
	endTime = clock();
	
	printf("%g\n", ((endTime - beginTime) / (double) (TESTS_TOTAL)) * (1000000 / (double) CLOCKS_PER_SEC));

	return 0;
}
