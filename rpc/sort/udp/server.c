#include <stdlib.h>

#include "sort.h"

int comparator(const void *x, const void *y)
{
	return *(int *)y - *(int *)x;
}

sort_out *sort_proc_1_svc(sort_in *vetor, struct svc_req *rqstp)
{
	static sort_out vetorResposta;
#ifdef DEBUG
	static int i = 0;
	printf("server: %i\n", i++);
	int j;
	for(j = 0; j < VECTOR_SIZE; j++)
		printf("%i ", vetor->vetor[j]);
	printf("\n");
#endif
	qsort(&vetor->vetor, VECTOR_SIZE, sizeof(int), comparator);
	memcpy(&vetorResposta.vetor, &vetor->vetor, VECTOR_SIZE * sizeof(int));
	return &vetorResposta;
}

