#include "null.h"

null_out *null_proc_1_svc(null_in *dumb, struct svc_req *rqstp)
{
	static null_out out = {'B'};
#ifdef DEBUG
	static int i = 0;
	printf("server: %i\n", i++);
#endif
	return &out;
}

