struct null_in {
	char dumb;
};

struct null_out {
	char dumb;
};

program NULL_PROG {
	version NULL_VERS {
		null_out NULL_PROC(null_in) = 1;
	} = 1;
} = 0x20000000;

