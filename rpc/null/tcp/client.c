#include <time.h>
#include <assert.h>
#include "null.h"

int main(int argc, char **argv)
{
	int i;
	CLIENT *client;
	null_in dumb = {'A'};
	double beginTime, endTime;

	if(argc < 3) {
		printf("Usage: %s <adress> <port>\n", argv[0]);
		exit(1);
	}

	beginTime = clock();
	for(i = 0; i < TESTS_TOTAL; i++) {
#ifdef DEBUG
		printf("client: %i\n", i);
#endif

		client = clnt_create(argv[1], NULL_PROG, NULL_VERS, SOCKET_TYPE);
		assert(client);
		assert(null_proc_1(&dumb, client));
		clnt_destroy(client);
	}
	endTime = clock();
	
	printf("%g\n", ((endTime - beginTime) / (double) (TESTS_TOTAL)) * (1000000 / (double) CLOCKS_PER_SEC));

	return 0;
}
