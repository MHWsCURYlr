#!/usr/bin/ruby
lines = IO.read(ARGV[0]).split("\n");
min = lines.inject{ |x, y| [x.to_i, y.to_i].min }
max = lines.inject{ |x, y| [x.to_i, y.to_i].max }
media = lines.inject{ |x, y| x.to_i + y.to_i }.to_f / lines.size;
desvio = Math.sqrt(lines.inject(0){ |x, y| x.to_i + (y.to_i - media) ** 2 } / (lines.size - 1))
coef = 2.262 * desvio / Math.sqrt(lines.size)
#print sprintf("  & %i & %i & %0.2f & %0.2f \\\\\n", min, max, media, desvio)
print sprintf("  & %0.2f & %0.2f & [%0.2f, %0.2f] \\\\\n", media, desvio, media - coef, media + coef)

