public class Server
	extends java.rmi.server.UnicastRemoteObject 
	implements Interface { 

	public Server() 
		throws java.rmi.RemoteException { 
		super(); 
	} 

	public int[] sort_proc(int[] vetor) {
		java.util.Arrays.sort(vetor);
		for(int i = 0, j = vetor.length - 1; i < j; i++, j--) {
			int temp = vetor[i];
			vetor[i] = vetor[j];
			vetor[j] = temp;
		}
		return vetor;
	}

	public static void main(String args[]) throws Exception{
		Interface server;

		if(args.length < 1) {
			System.out.println("Usage: java Server <address>");
			System.exit(1);
		}

		server = new Server();
		java.rmi.Naming.rebind("rmi://" + args[0] + "/Server", server);
	}
} 

