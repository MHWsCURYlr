public class Client { 
	public static void main(String[] args) throws Exception { 
		final int TESTS_TOTAL = 1000, VECTOR_SIZE = 250;
		final boolean DEBUG = false;
		Interface server;
		int[] vetor = new int[VECTOR_SIZE], vetorResposta = new int[VECTOR_SIZE];
		long beginTime, endTime;

		if(args.length < 1) {
			System.out.println("Usage: java Client <address>");
			System.exit(1);
		}

		server = (Interface) java.rmi.Naming.lookup("rmi://" + args[0] + "/Server");

		for(int i = 0; i < vetor.length; i++)
			vetor[i] = i;

		beginTime = System.nanoTime();
		for(int i = 0; i < TESTS_TOTAL; i++) {
			if(DEBUG) {
				System.out.println("client: " + i);
			}
			vetorResposta = server.sort_proc(vetor);
			if(DEBUG) {
				for(int j = 0; j < vetorResposta.length; j++)
					System.out.print(vetorResposta[j] + " ");
				System.out.println("");
			}
		}
		endTime = System.nanoTime();

		System.out.println((endTime - beginTime) / (double) (TESTS_TOTAL * 1000));
	} 
} 
