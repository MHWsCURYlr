public class Server
	extends java.rmi.server.UnicastRemoteObject 
	implements Interface { 

	public Server() 
		throws java.rmi.RemoteException { 
		super(); 
	} 

	public char null_proc(char dumb) {
		char res = 'B';
		return res; 
	}

	public static void main(String args[]) throws Exception{
		Interface server;

		if(args.length < 1) {
			System.out.println("Usage: java Server <address>");
			System.exit(1);
		}

		server = new Server();
		java.rmi.Naming.rebind("rmi://" + args[0] + "/Server", server);
	}
} 

