public class Client { 
	public static void main(String[] args) throws Exception { 
		final int TESTS_TOTAL = 1000;
		final boolean DEBUG = false;
		Interface server;
		char dumb = 'A';
		long beginTime, endTime;

		if(args.length < 1) {
			System.out.println("Usage: java Client <address>");
			System.exit(1);
		}

		server = (Interface) java.rmi.Naming.lookup("rmi://" + args[0] + "/Server");

		beginTime = System.nanoTime();
		for(int i = 0; i < TESTS_TOTAL; i++) {
			if(DEBUG) {
				System.out.println("client: " + i);
			}
			server.null_proc(dumb);
		}
		endTime = System.nanoTime();

		System.out.println((endTime - beginTime) / (double) (TESTS_TOTAL * 1000));
	} 
} 
