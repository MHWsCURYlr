#!/bin/bash
dir_names=("tcp/null" "tcp/sort" "udp/null" "udp/sort" "rpc/null/tcp" "rpc/null/udp" "rpc/sort/tcp" "rpc/sort/udp" "rmi/null" "rmi/sort")
for d in {0..9}
do
	dir_name=${dir_names[$d]}
	echo "$dir_name"
	mv "$dir_name/out.txt" "$dir_name/out.bak.txt"
	for j in {0..3}
	do
		#echo "$j"
		for i in {0..9}
		do
			#echo "$i"
			if [ "$d" = "8" ] || [ "$d" = "9" ]
			then
				cd "$dir_name"
				#java Client 127.0.0.1:808"$d" >> "out.txt"
				java Client 143.54.12.150:808"$d" >> "out.txt"
				cd "../../"
			else
				#./"$dir_name/client" 127.0.0.1 808"$d" >> "$dir_name/out.txt"
				./"$dir_name/client" 143.54.12.150 808"$d" >> "$dir_name/out.txt"
			fi
		done
		sleep 60
	done
	./calc.rb "$dir_name/out.txt"
done

