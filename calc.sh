#!/bin/bash
dir_names=("tcp/null" "tcp/sort" "udp/null" "udp/sort" "rpc/null/tcp" "rpc/null/udp" "rpc/sort/tcp" "rpc/sort/udp" "rmi/null" "rmi/sort")
for d in {0..9}
do
	dir_name=${dir_names[$d]}
	./calc.rb "$dir_name/out.txt"
done

