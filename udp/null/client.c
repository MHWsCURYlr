#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include <time.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <unistd.h>

int main(int argc, char **argv)
{
	int i, mySocket, serverAnswerSize;
	struct sockaddr_in server, serverAnswer;
	double beginTime, endTime;
	char byte = 'A';
	
	if(argc < 3) {
		printf("Usage: %s <address> <port>\n", argv[0]);
		exit(1);
	}

	memset(&server, 0, sizeof(server));
	server.sin_family = AF_INET;
	server.sin_addr.s_addr = inet_addr(argv[1]);
	server.sin_port = htons(atoi(argv[2]));

	serverAnswerSize = sizeof(serverAnswer);

	beginTime = clock();
	for(i = 0; i < TESTS_TOTAL; i++) {
#ifdef DEBUG
		printf("client: %i\n", i);
#endif

		mySocket = socket(AF_INET, SOCK_DGRAM, 0);
		assert(mySocket != -1);

		assert(sendto(mySocket, &byte, 1, 0, (struct sockaddr *) &server, sizeof(server)) == 1);
		assert(recvfrom(mySocket, &byte, 1, 0, (struct sockaddr *) &serverAnswer, (unsigned int *) &serverAnswerSize) == 1);
		assert(server.sin_addr.s_addr == serverAnswer.sin_addr.s_addr);
		
		//assert(connect(mySocket, (struct sockaddr *) &server, sizeof(server)) == 0);
		//assert(send(mySocket, &byte, 1, 0) == 1);
		//assert(recv(mySocket, &byte, 1, 0) == 1);
		//assert(send(mySocket, NULL, 0, 0) == 0);
		//assert(recv(mySocket, NULL, 0, 0) == 0);

		close(mySocket);
	}
	endTime = clock();

	printf("%g\n", ((endTime - beginTime) / (double) (TESTS_TOTAL)) * (1000000 / (double) CLOCKS_PER_SEC));

	return 0;
}
