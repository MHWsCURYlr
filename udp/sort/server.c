#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <string.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <errno.h>

int comparator(const void *x, const void *y)
{
	return *(int *)y - *(int *)x;
}

int main(int argc, char **argv)
{
	int mySocket, clientLenght;
	struct sockaddr_in server, client;;
	int vetor[VECTOR_SIZE];
	int vectorSize = VECTOR_SIZE * sizeof(int);

	if(argc < 2) {
		printf("Usage: %s <port>\n", argv[0]);
		exit(1);
	}

	mySocket = socket(AF_INET, SOCK_DGRAM, 0);
	assert(mySocket != -1);

	memset(&server, 0, sizeof(server));
	server.sin_family = AF_INET;
	server.sin_addr.s_addr = htonl(INADDR_ANY);
	server.sin_port = htons(atoi(argv[1]));

	assert(bind(mySocket, (struct sockaddr *) &server, sizeof(server)) == 0);
	//assert(listen(mySocket, 1) == 0);

	clientLenght = sizeof(client);
	//for(i = 0; i < TESTS_TOTAL; i++) {
	while(1) {
#ifdef DEBUG
		printf("server: %i\n", i);
#endif
		//clientSocket = accept(mySocket, (struct sockaddr *) &client, (unsigned int *) &clientLenght);
		//printf("errno: %i\n", errno);
		//assert(clientSocket != -1);

		assert(recvfrom(mySocket, vetor, vectorSize, 0, (struct sockaddr *) &client, (unsigned int *) &clientLenght) == vectorSize);
		qsort(vetor, VECTOR_SIZE, sizeof(int), comparator);
		assert(sendto(mySocket, vetor, vectorSize, 0, (struct sockaddr *) &client, clientLenght) == vectorSize);

		//assert(recv(clientSocket, &byte, 1, 0) == 1);
		//assert(send(clientSocket, &byte, 1, 0) == 1);
		//assert(recv(clientSocket, NULL, 0, 0) == 0);
		//assert(send(clientSocket, NULL, 0, 0) == 0);
		
		//close(clientSocket);
	}
		
	//close(clientSocket);
	close(mySocket);

	return 0;
}

